
    var counter = 1;
    var todos = []

    const Paragraph = (props) => {
      return <div>
        <p>Todos:</p>
        <ul className="list-group">
          { props.todos.map( (todo, index) => <li key={todo.id}>{ `${index+1}. ${todo.title}` } </li> ) }
        </ul>
      </div>
    }
    
    const App = (props) => <div> Test
      <Paragraph title={props.title || "Test"} todos={props.todos} />  
    </div>;


    setInterval(() => {

      todos.unshift({
        id: counter++,
        title: 'Todo ' + counter
      })

      ReactDOM.render( <App todos={todos} />  , document.getElementById('root'))

    }, 500)
