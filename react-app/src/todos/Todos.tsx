import * as React from 'react';
import { Todo } from './todo.interface';
import { TodoItem } from './TodoItem';

export class Todos extends React.Component<{
  title: string,
  todos: Todo[],
  onAdd: (title:string) => any,
  onToggle: (id:number) => any,
  onRemove: (id:number) => any
}>{

  static defaultProps = {
    title: 'Todos'
  }

  render() {
    return <div>
      <h2>
        {this.props.title}
      </h2>

      {this.props.todos.length? 
      <ul className="list-group">
        {this.props.todos.map(todo => 
          <TodoItem key={todo.id} todo={todo}
          onRemove={()=>{}}
          onToggle={()=>{}}
          />)}
      </ul> : 'No items found' }
      
      {this.props.children}
    </div>
  }

}