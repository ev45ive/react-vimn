import { Todo } from "./todo.interface";
import * as React from 'react';

export const TodoItem = ({ todo, onRemove, onToggle }: { 
  todo: Todo
  onRemove: (id:number) => void,
  onToggle: (id:number) => void
}) => (
  <li className="list-group-item">

    <input type="checkbox" 
      checked={todo.completed} 
      onChange={ () => onToggle(todo.id)}
    />

    {' ' + todo.title}

    <span className="close"
      onClick={ () => onRemove(todo.id) }
    > &times;</span>
  </li>
)