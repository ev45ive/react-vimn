import * as React from 'react';
import { ChangeEvent, KeyboardEvent } from 'react';

export class TodoForm extends React.Component<{
  onAdd: (title: string) => any;
}> {


  state = {
    newTitle: ''
  }


  onChange = (e: ChangeEvent<HTMLInputElement>) => {

    this.setState({
      newTitle: e.target.value
    })

  }

  onEnter = (event: KeyboardEvent<HTMLInputElement>) => {
    if (event.keyCode == 13) {
      this.addTodo()
    }
  }

  addTodo = () => {
    this.props.onAdd(this.state.newTitle)

    this.setState({
      newTitle: ''
    })
  }

  componentDidMount(){
    // console.log('did mount ref');
    // (this.refs['test'] as HTMLInputElement).focus()
    this.InputRef.focus()
  }

  componentWillUnmount(){
    
  }

  InputRef:HTMLInputElement

  render() {
    return (
      <div className="input-group mt-3">
        <input type="text" className="form-control" value={this.state.newTitle} onChange={this.onChange}
          onKeyUp={this.onEnter} ref={(elem:HTMLInputElement)=> this.InputRef = elem} />

        <span className="input-group-append">
          <span className="input-group-text">{this.state.newTitle.length} </span>
          <button className="btn btn-outline-secondary" onClick={this.addTodo}>Add</button>
        </span>

      </div>
    )
  }
}