import { AnyAction, combineReducers } from "redux";
import { Todo } from "../todos/todo.interface";


enum ActionType {
  ADD_TODO = 'ADD_TODO',
  ARCHIVE_TODO = 'ARCHIVE_TODO',
  REMOVE_TODO = 'REMOVE_TODO',
  TOGGLE_TODO = 'TOGGLE_TODO',
}

export const addTodo = (todo: Todo) => ({
  type: ActionType.ADD_TODO,
  payload: {
    todo
  }
})

export const createTodo = (title: string) => ({
  type: ActionType.ADD_TODO,
  payload: {
    todo: {
      id: Date.now(),
      title,
      completed: false
    }
  }
})

export const removeTodo = (id: number) => ({
  type: ActionType.REMOVE_TODO,
  payload: { id }
})

export const toggleTodo = (id: number) => ({
  type: ActionType.TOGGLE_TODO,
  payload: { id }
})

export const archiveTodo = (id: number) => ({
  type: ActionType.ADD_TODO,
  payload: {
    id
  }
})

export const getTodosList = (listName:string) => (state:TodosState):Todo[] => {
  return state.orders[listName].map( (id:number) => state.entities[id] )
}

interface Entities<T> {
  [id: number]: T
}
type Order = number[]


interface TodosState {
  entities: Entities<Todo>,
  orders: {
    todos: Order,
    archived: Order,
  }
}

const entityReducer = (state: Entities<Todo> = {}, action: AnyAction): Entities<Todo> => {

  switch (action.type) {
    case ActionType.ADD_TODO: {
      let todo = action.payload.todo;
      return {
        ...state,
        [todo.id]: todo
      }
    }
    // case ActionType.REMOVE_TODO: // Is there TODO on any of the lists!?
    case ActionType.TOGGLE_TODO: {
      let todo = state[action.payload.id]

      return {
        ...state,
        [todo.id]: { ...todo, completed: !todo.completed }
      }
    }
    default:
      return state
  }
}

const orderReducer = (collectionName: string, defaultCollection = false) => (state: Order = [], action: AnyAction): Order => {

  // Skip if not in correct collection
  if( !defaultCollection ){
    return state
  }

  switch (action.type) {
    case ActionType.ADD_TODO: {
      return [...state, action.payload.todo.id ]
    }
    case ActionType.REMOVE_TODO: {
      return state.filter( id => id != action.payload.todo.id )
    }
    default:
      return state
  }
}

export const todos = combineReducers<TodosState>({
  entities: entityReducer,
  orders: combineReducers({
    todos: orderReducer('todos', true),
    archived: orderReducer('archived')
  })
})


// export const todos = (state: TodosState = {
//   entities: {},
//   orders: {
//     todos: [],
//     archived: []
//   }
// }, action: AnyAction): TodosState => {

//   return {
//     ...state,
//     entities: entityReducer(state.entities,action),
//     orders:{
//       todos: orderReducer(state.orders.todos,action),
//       archived: orderReducer(state.orders.archived, action)
//     }
//   }


//   // switch (action.type) {
//   //   case ActionType.ADD_TODO:
//   //   case ActionType.REMOVE_TODO:
//   //   case ActionType.TOGGLE_TODO:
//   //     return {
//   //       ...state,
//   //     }

//   //   case ActionType.ARCHIVE_TODO:


//   //   default:
//   //     return state
//   // }

// };





/*

switch (action.type) {
    case ActionType.ADD_TODO:
      return {
        ...state,
        entities:
          todos: [...state.todos, < Todo > action.payload.todo],
  }

    case ActionType.REMOVE_TODO:
return {
  ...state,
  todos: state.todos.filter(
    todo => todo.id != action.payload.id
  )
}
    case ActionType.TOGGLE_TODO:
return {
  ...state,
  todos: state.todos.map(todo => {
    if (todo.id == action.payload.id) {
      return {
        ...todo,
        completed: !todo.completed
      }
    }
    return todo
  })
}

    case ActionType.ARCHIVE_TODO:
let todo = <Todo>state.todos.find(
  todo => todo.id == action.payload.id
)

return {
  ...state,
  todos: state.todos.filter(
    todo => todo.id == action.payload.id
  ),
  archived: [...state.todos, todo]
}
    default:
return state;
  }

  */
