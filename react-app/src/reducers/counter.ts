
enum ActionType {
  INCREMENT = 'INCREMENT',
  DECREMENT = 'DECREMENT'
}

export const Increment = (payload = 1) => (
  { type: ActionType.INCREMENT, payload }
)

export const Decrement = (payload = 1) => (
  { type: ActionType.DECREMENT, payload }
)

window['Increment'] = Increment

export interface CounterAction {
  type: ActionType;
  payload: number
}

export const counter = (state = 0, action: CounterAction) => {
  switch (action.type) {
    case ActionType.INCREMENT:
      return state + action.payload
    case ActionType.DECREMENT:
      return state - action.payload
    default:
      return state;
  }
};