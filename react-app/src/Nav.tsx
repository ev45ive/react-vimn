import * as React from 'react'
import { ReactElement } from 'react';

interface TabProps {
  navKey:string;
  label:string;
}

export class NavTab extends React.Component<TabProps>{
  
  render(){
    return this.props.children
  }
}

export class Nav extends React.Component {

  state = {
    selected: 'todos'
  }

  select(selected: string) {
    this.setState({ selected })
  }

  render() {
    return <div>
      <ul className="nav nav-pills">
        {
          React.Children.toArray(this.props.children).map( (child:ReactElement<TabProps>) => {
            const key = child.props.navKey
            const label = child.props.label
            const selected = key == this.state.selected

            return <li className="nav-item" key={key}>
              <a
                className={`nav-link ${selected? 'active':''}`}
                onClick={() => this.select(key)}
              >
                {label}
              </a>
            </li>
          })
        }
      </ul>
      { React.Children.toArray(this.props.children).filter( (child:ReactElement<TabProps>) => {
        // console.log(child)
        return child.props.navKey == this.state.selected
      }) }
    </div>
  }
}

