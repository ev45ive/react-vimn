
import { Todos } from '../todos/Todos';
import { AppState } from '../store';
import { createTodo, removeTodo, toggleTodo, getTodosList } from '../reducers/todos';

import { bindActionCreators } from 'redux'
import { connect, Dispatch } from 'react-redux'

const mapStateToProps = (state: AppState) => ({
  title: 'Archive',
  todos: getTodosList('archived')(state.todos)
})

const mapDispatchToProps = (dispatch: Dispatch<AppState>) => bindActionCreators({
  onAdd: createTodo,
  onToggle: toggleTodo,
  onRemove: removeTodo
}, dispatch)


export const ArchivedContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Todos)