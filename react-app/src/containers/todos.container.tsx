
import { Todos } from '../todos/Todos';
import { AppState } from '../store';
import { createTodo, removeTodo, toggleTodo, getTodosList} from '../reducers/todos';

import { bindActionCreators } from 'redux'
import { connect, Dispatch } from 'react-redux'

const mapStateToProps = (state:AppState) => ({
  title: 'Todos',
  todos: getTodosList('todos')(state.todos)
})

const mapDispatchToProps = (dispatch:Dispatch<AppState>) => bindActionCreators({
  onAdd: createTodo,
  onToggle: toggleTodo,
  onRemove: removeTodo
}, dispatch)


export const TodosContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)( Todos )

// const mapDispatchToProps = (dispatch:Dispatch<AppState>) => ({
//   onAdd: (title:string) => dispatch(createTodo(title)),
//   onToggle: (id:number) => dispatch(toggleTodo(id)),
//   onRemove:  (id:number) => dispatch(removeTodo(id)),
// })


/*


  render() {
    return (
      <Nav>
        <NavTab navKey="todos" label="Todos">
          <Todos todos={this.state.todos}
            onAdd={this.addTodo}
            onToggle={this.toggleTodo}
            onRemove={this.removeTodo}
          >
            <TodoForm onAdd={this.addTodo} />
          </Todos>
        </NavTab>

        <NavTab navKey="archived" label="Archived">
          <Todos todos={this.state.archived} title="Archived"
            onAdd={this.addTodo}
            onToggle={this.toggleTodo}
            onRemove={this.removeTodo} />
        </NavTab>

      </Nav>
    );
  }
}

*/