import * as React from 'react';
import { User } from '../users/user.interface';
import { UserDetails } from '../users/user-details.components'
import * as PropTypes from 'prop-types'
import { UsersEventEmitter } from '../users-event-emitter';

export class UserContainer extends React.Component {

  state = {
    user: null
  } as { user: User | null }

  context:{
    userEvents: UsersEventEmitter
  }

  static contextTypes = {
    userEvents: PropTypes.instanceOf(UsersEventEmitter)
  }

  constructor(props:any){
    super(props)
  }

  componentDidMount(){
    this.context.userEvents.on('selected', userId => {
      fetch('http://localhost:9000/users/'+userId)
      .then(response => response.json())
      .then(user => {
        this.setState({user})
      })
    })
  }
  
  onChange = (event:React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      user: {
        ...this.state.user,
        [event.target.name]: event.target.value
      }
    })
  }

  saveUser = () => {
    const user = this.state.user
    if(user){
      fetch(`http://localhost:9000/users/${user.id}`,{
        method:'PUT',
        headers:{
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(user)
      }).then(()=>{
        this.context.userEvents.emit('selected', user)
      })
    }
  }

  render() {
    return this.state.user? 
      <UserDetails user={this.state.user}
      onSave={this.saveUser}
      onChange={this.onChange}/> 
    : 'Please select user'
  }

}