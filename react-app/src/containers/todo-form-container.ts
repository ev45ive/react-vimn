import { createTodo } from "../reducers/todos";
import { bindActionCreators } from "redux";
import { Dispatch, connect } from "react-redux";
import { TodoForm } from "../todos/TodoForm";
import { AppState } from "../store";


export const TodoFormContainer = connect(
  null,
  (dispatch:Dispatch<AppState>) => bindActionCreators({
    onAdd: createTodo
  },dispatch)
)(TodoForm)