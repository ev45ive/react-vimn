import * as React from 'react';
import { User } from '../users/user.interface';
import { UsersList } from '../users/users-list.component';
import { UsersEventEmitter } from '../users-event-emitter';
import * as PropTypes from 'prop-types'



export class UsersListContainer extends React.Component<{}, {
  users: User[],
  selected?: number
}>{

  state = {
    users: [
      {
        id: 1, name: 'Alice', email: 'alice@wonderland'
      }
    ],
    selected: undefined
  }

  context: {
    userEvents: UsersEventEmitter
  }

  static contextTypes = {
    userEvents: PropTypes.instanceOf(UsersEventEmitter)
  }

  fetchUsers() {
    return fetch('http://localhost:9000/users')
      .then(response => response.json())
      .then(users => {
        this.setState({ users })
        return users
      })
  }

  componentDidMount() {
    this.context.userEvents.on('selected', selected => {
      this.setState({ selected })
    })
    this.fetchUsers().then(users => {
      this.context.userEvents.emit('selected', users[0].id)
    })
  }

  selectUser = (id: number) => {

    const user = this.state.users.find(user => user.id == id)
    if(user){
      this.context.userEvents.emit('selected',
        user.id 
      )
    }
  }

  render() {
    return <UsersList users={this.state.users}
      selected={this.state.selected}
      onSelect={this.selectUser}
    />
  }

}