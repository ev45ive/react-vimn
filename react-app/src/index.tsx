import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

import 'bootstrap/dist/css/bootstrap.css';

import { store } from './store'
import { Provider } from 'react-redux';

import { HashRouter as Router, Route } from 'react-router-dom'

ReactDOM.render(
<Router>
  <Provider store={store}>
    <App title="test" />
  </Provider>
</Router>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
