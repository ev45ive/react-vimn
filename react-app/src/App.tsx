import * as React from 'react';
// import { TodosContainer } from './containers/todos.container';
// import { UsersListContainer } from './containers/users.container';
// import { UserContainer } from './containers/user.container';

import * as PropTypes from 'prop-types'
import { UsersEventEmitter } from './users-event-emitter';
import { TodosContainer } from './containers/todos.container';
import { ArchivedContainer } from './containers/archived.container';
import { TodoFormContainer } from './containers/todo-form-container';
import { Route, Switch, Redirect } from 'react-router-dom';
import { UsersListContainer } from './containers/users.container';

interface AppProps {
  title?: string;
}


class App extends React.Component<AppProps> {

  events: UsersEventEmitter

  constructor(props: AppProps) {
    super(props)
    this.events = new UsersEventEmitter()
  }

  getChildContext() {
    return { userEvents: this.events };
  }

  static childContextTypes = {
    userEvents: PropTypes.instanceOf(UsersEventEmitter)
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col">
            <Route path="/" exact render={() => <Redirect to="/users" />} />
            <Route path="/users" component={UsersListContainer} />
            <Route path="/todos" component={TodosContainer} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
