import * as React from 'react';
import { User } from './user.interface';
import { Route } from 'react-router';


export class UsersList extends React.Component<{
  users: User[],
  title?: string,
  selected?: number,
  onSelect: (id: number) => void
}>{

  static defaultProps = {
    title: 'Users',
  }

  render() {
    return <div className="row">
      <div className="col">
        <h3> {this.props.title} </h3>
        <ul className="list-group">
          {
            this.props.users.map(user =>
              <li key={user.id}
                onClick={() => this.props.onSelect(user.id)}
                className={`list-group-item ${this.props.selected == user.id ? 'active' : ''}`}>
                {user.name}
              </li>
            )}
        </ul>
      </div>
      <div className="col">
          <Route path="/users/:id" exact render={ props => {
            console.log(props);
            return 'ups'
          }} />
      </div>
    </div>
  }

}