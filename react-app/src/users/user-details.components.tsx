import * as React from 'react';
import { User } from './user.interface';
import { ChangeEvent } from 'react';


export class UserDetails extends React.Component<{
  user: User,
  title?: string,
  onSave: () => void,
  onChange: (e:ChangeEvent<HTMLInputElement>) => void
}>{

  static defaultProps = {
    title: 'User'
  }


  render() {
    const user = this.props.user;
    return <div>
      <h3> {this.props.title} </h3>
      <div>
        <div className="form-group">
          <label>Name</label>
          <input className="form-control" onChange={this.props.onChange} value={user.name} name="name" />
        </div>
        <div className="form-group">
          <label>E-mail</label>
          <input className="form-control" onChange={this.props.onChange} value={user.email} name="email" />
        </div>
        <div className="form-group">
          <button onClick={this.props.onSave} className="btn btn-success">Save</button>
        </div>
      </div>
    </div>
  }
}