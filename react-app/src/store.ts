import { createStore, combineReducers } from 'redux'
import { counter } from './reducers/counter';
import { todos } from './reducers/todos';
// import { Todo } from './todos/todo.interface';

export interface AppState {
  counter: number;
  todos: {
    entities: {},
    orders: {
      todos: number[],
      archived: number[]
    }
  }
}

const initialState = {
  counter: 0,

  todos: {
    entities: {
      1: {
        id: 1,
        title: 'Redux Todo',
        completed: true
      },
    },
    orders: {
      todos: [1],
      archived: []
    }
  }
}


const reducer = combineReducers({
  counter: counter,
  todos: todos
})

export const store = createStore(reducer, initialState)

